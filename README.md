# Equipe de Tutoriais

## Grupo 7 - membros
* Rogério Amorim Marinho Junior (LIDER)
* Thiago Nonato Souza (DOCER)
* Tony William Maia de Freitas (DEVER)
* Jhonata Leandro Bernardes Paiva (DOCER)
* Ismael Cirilo Junior (DOCER)

## Objetivos:
* Criar tutoriais teórico-práticos úteis para o desenvolvimento do projeto
* Requisitos: utilizar jupyter como ferramenta de criação de tutoriais:
    * Exemplos: https://gitlab.com/marceloakira/tutorial

## Tarefas da sprint 1 (02/09 - 08/09): 
* Definir tutoriais para cada membro
* Aprender a utilizar jupyter e revisar tutoriais
* 

## Tutorial Instalação do Jupyter - https://gitlab.com/tetrinetjs/tutorials-g7/blob/master/Tutorial_IsntallJupyter.ipynb
* Simples tutorial mostrando a instalação, como verificar se o Jupyter foi instalado e como iniciar o mesmo.

## Tutorial de Programação Assíncrona em Node.js - https://gitlab.com/tetrinetjs/tutorials-g7/blob/master/Programa%C3%A7%C3%A3o%20Ass%C3%ADncrona%20em%20Node.js%20.ipynb
* Um tutorial em um nível intermediário onde falamos a respeito do Node.js e a programação assíncrona usando ele. Neste tutorial falamos sobre os comandos npm, falamos sobre as variáveis do Node.js e sua organização em relação aos módulos, por fim falamos do assíncronismo em Node.js.
